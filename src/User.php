<?php

namespace Mayohaus\Aserver;

class User
{
    private $id;

    private $name;

    private $mobile;

    private $email;

    private $nickname;

    private $portrait;

    private $gender;

    private $createdAt;

    private $activatedAt;


    public function __construct( $id = null )
    {
        $this->id = $id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName( $name )
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setMobile( $mobile )
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    public function setEmail( $email )
    {
        $this->email = $email;

        return $this;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setNickname( $nickname )
    {
        $this->nickname = $nickname;

        return $this;
    }

    public function getNickname()
    {
        return $this->nickname;
    }

    public function setPortrait( $portrait )
    {
        $this->portrait = $portrait;

        return $this;
    }

    public function getPortrait()
    {
        return $this->portrait;
    }

    public function setGender( $gender )
    {
        $this->gender = $gender;

        return $this;
    }

    public function getGender()
    {
        return $this->gender;
    }

    public function setCreatedAt( \DateTime $createdAt )
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setActivatedAt( \DateTime $activatedAt = null )
    {
        $this->activatedAt = $activatedAt;

        return $this;
    }

    public function getActivatedAt()
    {
        return $this->activatedAt;
    }
}
