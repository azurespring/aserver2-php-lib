<?php

namespace Mayohaus\Aserver\Exception;

class BadCredentialException extends Exception
{
    const ERRNO = 2164261865;
}
