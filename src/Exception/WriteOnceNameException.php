<?php

namespace Mayohaus\Aserver\Exception;

class WriteOnceNameException extends \RuntimeException
{
    const ERRNO = 2164261885;
}
