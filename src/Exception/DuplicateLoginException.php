<?php

namespace Mayohaus\Aserver\Exception;

class DuplicateLoginException extends \RuntimeException
{
    const ERRNO = 2164261875;
}
