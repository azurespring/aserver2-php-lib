<?php

namespace Mayohaus\Aserver\Exception;

class FalseLoginException extends Exception
{
    const ERRNO = 2164261866;
}
