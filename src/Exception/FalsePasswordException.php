<?php

namespace Mayohaus\Aserver\Exception;

class FalsePasswordException extends Exception
{
    const ERRNO = 2164261867;
}
