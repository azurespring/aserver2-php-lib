<?php

namespace Mayohaus\Aserver\Exception;

class MissingArgumentException extends Exception
{
    const ERRNO = 2164260866;
}
