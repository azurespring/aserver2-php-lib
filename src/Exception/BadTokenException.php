<?php

namespace Mayohaus\Aserver\Exception;

class BadTokenException extends Exception
{
    const ERRNO = 2164262865;
}
