<?php

namespace Mayohaus\Aserver;

use GuzzleHttp\Client as Guzzle;
use Psr\Http\Message\ResponseInterface;

class Client
{
    const FEMALE    = 'female';
    const MALE      = 'male';

    private $guzzle;


    public function __construct( Guzzle $guzzle )
    {
        $this->guzzle = $guzzle;
    }

    public function register( User $user, $password, $hang = false )
    {
        $document = array_merge(
            array( 'token' => null ),
            $this->sanitize(
                $this->guzzle->request('POST', '/v1/register', array(
                    'form_params' => array_merge(
                        $this->marshal( $user ),
                        array( 'password'  => $password,
                               'active'    => $hang ? 'false' : 'true' )
                    )
                ))
            )
        );

        return array( $document['token'],
                      $this->unmarshal($document['user']) );
    }

    public function login( $login, $password )
    {
        $document = $this->sanitize(
            $this->guzzle->request('POST', '/v1/login', array(
                'form_params' => array(
                    'ident'     => $login,
                    'password'  => $password
                )
            ))
        );

        return array( $document['token'],
                      $this->unmarshal($document['user']) );
    }

    public function logout( $token )
    {
        $this->sanitize(
            $this->guzzle->request('POST', '/v1/logout', array(
                'form_params' => array( 'token' => $token )
            ))
        );
    }

    public function refresh( $token )
    {
        $document = $this->sanitize(
            $this->guzzle->request('GET', '/v1/profile', array(
                'query' => array( 'token' => $token )
            ))
        );

        return $this->unmarshal( $document['user'] );
    }

    public function flush( $token, User $user )
    {
        $document = $this->sanitize(
            $this->guzzle->request('PUT', '/v1/profile', array(
                'form_params' => array_merge(
                    array( 'token' => $token ),
                    array_intersect_key(
                        $this->marshal( $user ),
                        array_fill_keys(
                            array( 'name',
                                   'mobile',
                                   'email',
                                   'nickname',
                                   'portrait',
                                   'gender' ),
                            null
                        )
                    )
                )
            ))
        );

        return $this->unmarshal( $document['user'] );
    }

    public function verify( $token, $password )
    {
        $this->sanitize(
            $this->guzzle->request('GET', '/v1/password', array(
                'query' => array(
                    'token'     => $token,
                    'password'  => $password
                )
            ))
        );
    }

    public function password( $token, $new, $old = false )
    {
        $this->sanitize(
            $old ?
            $this->guzzle->request('POST', '/v1/password', array(
                'form_params' => array(
                    'token'         => $token,
                    'new_password'  => $new,
                    'old_password'  => $old,
                )
            )) :
            $this->guzzle->request('PUT', '/v1/password', array(
                'form_params' => array(
                    'token'         => $token,
                    'password'      => $new,
                )
            ))
        );
    }

    public function su( $login )
    {
        $document = $this->sanitize(
            $this->guzzle->request('POST', '/v1/substitute', array(
                'form_params' => array( 'ident' => $login )
            ))
        );

        return array( $document['token'],
                      $this->unmarshal($document['user']) );
    }

    public function suid( $id )
    {
        $document = $this->sanitize(
            $this->guzzle->request('POST', '/v1/token', array(
                'form_params' => array( 'id' => $id )
            ))
        );

        return array( $document['token'],
                      $this->unmarshal($document['user']) );
    }

    public function inspect( array $logins )
    {
        $document = $this->sanitize(
            $this->guzzle->request('GET', '/v1/search', array(
                'query' => preg_replace(
                    '/%5B\d*?%5D/i',
                    '',
                    http_build_query(array( 'ident' => $logins ))
                )
            ))
        );

        return array_map(
            function ($profile) {
                return $this->unmarshal($profile);
            },
            $document['user']
        );
    }

    private function sanitize( ResponseInterface $response )
    {
        if ( $response->getStatusCode() != 200 )
            throw new Exception\HTTPException( $response->getReasonPhrase(),
                                               $response->getStatusCode() );

        $document = json_decode( $response->getBody(), true );
        if ( !$document )
            throw new Exception\JSONException();

        if ( $document['error'] ) {
            foreach (
                array(
                    'MissingArgumentException',
                    'BadCredentialException',
                    'FalseLoginException',
                    'FalsePasswordException',
                    'DuplicateLoginException',
                    'WriteOnceNameException',
                    'BadTokenException',
                ) as $klass
            ) {
                $klass = 'Mayohaus\\Aserver\\Exception\\' . $klass;
                if ( $klass::ERRNO != $document['error'] )
                    continue;

                throw new $klass( $document['message'],
                                  $document['error'] );
            }

            throw new Exception\Exception( $document['message'],
                                           $document['error'] );
        }

        return $document;
    }

    private function marshal( User $user )
    {
        return array_filter(array(
            'id'        => $user->getId(),
            'name'      => $user->getName(),
            'mobile'    => $user->getMobile(),
            'email'     => $user->getEmail(),
            'nickname'  => $user->getNickname(),
            'portrait'  => $user->getPortrait(),
            'gender'    => $user->getGender() ? self::MALE : self::FEMALE,

            'created_at'    => ( $user->getCreatedAt() ?
                                 $user->getCreatedAt()->format( \DateTime::ATOM ) :
                                 null ),
            'activated_at'  => ( $user->getActivatedAt() ?
                                 $user->getActivatedAt()->format( \DateTime::ATOM ) :
                                 null )
        ));
    }

    private function unmarshal( array $profile )
    {
        $profile = array_merge(
            array_fill_keys(
                array( 'name', 'mobile', 'email',
                       'nickname', 'portrait', 'gender',
                       'activated_at' ),
                null
            ),
            $profile
        );

        return
            (new User( $profile['id'] ))
            ->setName( $profile['name'] )
            ->setMobile( $profile['mobile'] )
            ->setEmail( $profile['email'] )
            ->setNickname( $profile['nickname'] )
            ->setPortrait( $profile['portrait'] )
            ->setGender(
                is_null($profile['gender']) ?
                null :
                self::MALE == $profile['gender']
            )
            ->setCreatedAt(
                new \DateTime($profile['created_at'])
            )
            ->setActivatedAt(
                is_null($profile['activated_at']) ?
                null :
                new \DateTime($profile['activated_at'])
            )
        ;
    }
}
