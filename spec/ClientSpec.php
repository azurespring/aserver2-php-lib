<?php

namespace spec\Mayohaus\Aserver;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use GuzzleHttp\Client as Guzzle;
use Psr\Http\Message\ResponseInterface;
use Mayohaus\Aserver\User;

class ClientSpec extends ObjectBehavior
{
    function let( Guzzle $guzzle )
    {
        $this->beConstructedWith( $guzzle );
    }

    function it_is_initializable( Guzzle $guzzle )
    {
        $this->shouldHaveType( 'Mayohaus\\Aserver\\Client' );
    }

    function it_should_register_inactive( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/register', array(
                'form_params' => array(
                    'name'      => 'alex',
                    'mobile'    => '+86 18900000000',
                    'email'     => 'alex@example.com',
                    'nickname'  => 'Alexandra',
                    'gender'    => 'female',
                    'password'  => 'secret',
                    'active'    => 'false',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"token": null, "user": {"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2016-01-27T19:09:06.408759", "activated_at": null, "email": "alex@example.com", "portrait": null, "nickname": "Alexandra", "id": 2}, "error": 0}
__JSON__
            )
        ;

        $user =
            ( new User() )
            ->setName( 'alex' )
            ->setMobile( '+86 18900000000' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
        ;
        $this->register( $user, 'secret', true )->shouldBeLike(
            array(
                null,
                (new User( 2 ))
                ->setName( 'alex' )
                ->setMobile( '+86 18900000000' )
                ->setEmail( 'alex@example.com' )
                ->setNickname( 'Alexandra' )
                ->setGender( false )
                ->setCreatedAt(new \DateTime( '2016-01-27T19:09:06.408759' ))
            )
        );
    }

    function it_should_register_active( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/register', array(
                'form_params' => array(
                    'name'      => 'alex',
                    'mobile'    => '+86 18900000000',
                    'email'     => 'alex@example.com',
                    'nickname'  => 'Alexandra',
                    'gender'    => 'female',
                    'password'  => 'secret',
                    'active'    => 'true',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"token": "ijA1DsTnEeWgrQgAJ7Szd6C9fHs=", "user": {"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2016-01-27T19:09:06.408759", "activated_at": "2016-01-27T19:17:23.955675", "email": "alex@example.com", "portrait": null, "nickname": "Alexandra", "id": 2}, "error": 0}
__JSON__
            )
        ;

        $user =
            ( new User() )
            ->setName( 'alex' )
            ->setMobile( '+86 18900000000' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
        ;
        $this->register( $user, 'secret' )->shouldBeLike(
            array(
                'ijA1DsTnEeWgrQgAJ7Szd6C9fHs=',
                (new User( 2 ))
                ->setName( 'alex' )
                ->setMobile( '+86 18900000000' )
                ->setEmail( 'alex@example.com' )
                ->setNickname( 'Alexandra' )
                ->setGender( false )
                ->setCreatedAt(new \DateTime( '2016-01-27T19:09:06.408759' ))
                ->setActivatedAt(new \DateTime( '2016-01-27T19:17:23.955675' ))
            )
        );
    }

    function it_should_login_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/login', array(
                'form_params' => array(
                    'ident'     => 'alex',
                    'password'  => 'secret',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"token": "_JDuzsRFEeWcggAWPgAAts1G_0A=", "user": {"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2014-03-17T22:34:35.400114", "activated_at": "2014-04-17T22:34:35.400114", "email": "alex@example.com", "portrait": "da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg", "nickname": "Alexandra", "id": 7}, "error": 0}
__JSON__
            )
        ;

        $this->login( 'alex', 'secret' )->shouldBeLike(array(
            '_JDuzsRFEeWcggAWPgAAts1G_0A=',
            (new User( 7 ))
            ->setName( 'alex' )
            ->setMobile( '+86 18900000000' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
            ->setPortrait( 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg' )
            ->setGender( false )
            ->setCreatedAt(new \DateTime( '2014-03-17T22:34:35.400114' ))
            ->setActivatedAt(new \DateTime( '2014-04-17T22:34:35.400114' ))
        ));
    }

    function it_should_logout_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/logout', array(
                'form_params' => array( 'token' => '_JDuzsRFEeWcggAWPgAAts1G_0A=' )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"error": 0}
__JSON__
            )
        ;

        $this->logout( '_JDuzsRFEeWcggAWPgAAts1G_0A=' );
    }

    function it_should_refresh_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('GET', '/v1/profile', array(
                'query' => array( 'token' => '_JDuzsRFEeWcggAWPgAAts1G_0A=' )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"user": {"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2014-03-17T22:34:35.400114", "activated_at": "2014-04-17T22:34:35.400114", "email": "alex@example.com", "portrait": "da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg", "nickname": "Alexandra", "id": 7}, "error": 0}
__JSON__
            )
        ;

        $this
            ->refresh( '_JDuzsRFEeWcggAWPgAAts1G_0A=' )
            ->shouldBeLike(
                (new User( 7 ))
                ->setName( 'alex' )
                ->setMobile( '+86 18900000000' )
                ->setEmail( 'alex@example.com' )
                ->setNickname( 'Alexandra' )
                ->setPortrait( 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg' )
                ->setGender( false )
                ->setCreatedAt(new \DateTime( '2014-03-17T22:34:35.400114' ))
                ->setActivatedAt(new \DateTime( '2014-04-17T22:34:35.400114' ))
            )
        ;
    }

    function it_should_refresh_with_bad_token( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('GET', '/v1/profile', array(
                'query' => array( 'token' => '_JDuzsRFEeWcggAWPgAAts1G_0A' )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"message": "bad token", "cause": [], "error": 2164262865}
__JSON__
            )
        ;

        $this
            ->shouldThrow( 'Mayohaus\\Aserver\\Exception\\BadTokenException' )
            ->during( 'refresh', array('_JDuzsRFEeWcggAWPgAAts1G_0A') )
        ;
    }

    function it_should_flush_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('PUT', '/v1/profile', array(
                'form_params' => array(
                    'token'         => '_JDuzsRFEeWcggAWPgAAts1G_0A=',
                    'name'          => 'alex',
                    'mobile'        => '+86 18901234567',
                    'email'         => 'alex@example.com',
                    'nickname'      => 'Alexandra',
                    'portrait'      => 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg',
                    'gender'        => 'female',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"user": {"name": "alex", "mobile": "+86 18901234567", "gender": "female", "created_at": "2014-03-17T22:34:35.400114", "activated_at": "2014-04-17T22:34:35.400114", "email": "alex@example.com", "portrait": "da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg", "nickname": "Alexandra", "id": 7}, "error": 0}
__JSON__
            )
        ;

        $user =
            (new User( 7 ))
            ->setName( 'alex' )
            ->setMobile( '+86 18901234567' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
            ->setPortrait( 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg' )
            ->setGender( false )
            ->setCreatedAt(new \DateTime( '2014-03-17T22:34:35.400114' ))
            ->setActivatedAt(new \DateTime( '2014-04-17T22:34:35.400114' ))
        ;
        $this->flush( '_JDuzsRFEeWcggAWPgAAts1G_0A=', $user )->shouldBeLike( $user );
    }

    function it_should_verify_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('GET', '/v1/password', array(
                'query' => array(
                    'token'         => '_JDuzsRFEeWcggAWPgAAts1G_0A=',
                    'password'      => 'secret',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"error": 0}
__JSON__
            )
        ;

        $this->verify( '_JDuzsRFEeWcggAWPgAAts1G_0A=', 'secret' );
    }

    function it_should_verify_with_false_password( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('GET', '/v1/password', array(
                'query' => array(
                    'token'         => '_JDuzsRFEeWcggAWPgAAts1G_0A=',
                    'password'      => 'secret',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"message": "false password", "cause": [], "error": 2164261867}
__JSON__
            )
        ;

        $this
            ->shouldThrow( 'Mayohaus\\Aserver\\Exception\\FalsePasswordException' )
            ->during( 'verify', array('_JDuzsRFEeWcggAWPgAAts1G_0A=', 'secret') )
        ;
    }

    function it_should_change_password_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/password', array(
                'form_params' => array(
                    'token'         => '_JDuzsRFEeWcggAWPgAAts1G_0A=',
                    'new_password'  => 's3cret',
                    'old_password'  => 'secret',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"error": 0}
__JSON__
            )
        ;

        $this->password( '_JDuzsRFEeWcggAWPgAAts1G_0A=', 's3cret', 'secret' );
    }

    function it_should_reset_password_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('PUT', '/v1/password', array(
                'form_params' => array(
                    'token'     => '_JDuzsRFEeWcggAWPgAAts1G_0A=',
                    'password'  => 's3cret',
                )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"error": 0}
__JSON__
            )
        ;

        $this->password( '_JDuzsRFEeWcggAWPgAAts1G_0A=', 's3cret' );
    }

    function it_should_su_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/substitute', array(
                'form_params' => array( 'ident' => 'alex' )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"token": "_JDuzsRFEeWcggAWPgAAts1G_0A=", "user": {"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2014-03-17T22:34:35.400114", "activated_at": "2014-04-17T22:34:35.400114", "email": "alex@example.com", "portrait": "da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg", "nickname": "Alexandra", "id": 7}, "error": 0}
__JSON__
            )
        ;

        $this->su( 'alex' )->shouldBeLike(array(
            '_JDuzsRFEeWcggAWPgAAts1G_0A=',
            (new User( 7 ))
            ->setName( 'alex' )
            ->setMobile( '+86 18900000000' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
            ->setPortrait( 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg' )
            ->setGender( false )
            ->setCreatedAt(new \DateTime( '2014-03-17T22:34:35.400114' ))
            ->setActivatedAt(new \DateTime( '2014-04-17T22:34:35.400114' ))
        ));
    }

    function it_should_suid_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('POST', '/v1/token', array(
                'form_params' => array( 'id' => 7 )
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"token": "_JDuzsRFEeWcggAWPgAAts1G_0A=", "user": {"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2014-03-17T22:34:35.400114", "activated_at": "2014-04-17T22:34:35.400114", "email": "alex@example.com", "portrait": "da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg", "nickname": "Alexandra", "id": 7}, "error": 0}
__JSON__
            )
        ;

        $this->suid( 7 )->shouldBeLike(array(
            '_JDuzsRFEeWcggAWPgAAts1G_0A=',
            (new User( 7 ))
            ->setName( 'alex' )
            ->setMobile( '+86 18900000000' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
            ->setPortrait( 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg' )
            ->setGender( false )
            ->setCreatedAt(new \DateTime( '2014-03-17T22:34:35.400114' ))
            ->setActivatedAt(new \DateTime( '2014-04-17T22:34:35.400114' ))
        ));
    }

    function it_should_inspect_ok( Guzzle $guzzle, ResponseInterface $response )
    {
        $guzzle
            ->request('GET', '/v1/search', array(
                'query' => 'ident=alex&ident=bob'
            ))
            ->shouldBeCalled()
            ->willReturn( $response )
        ;
        $response
            ->getStatusCode()
            ->willReturn( 200 )
        ;
        $response
            ->getBody()
            ->willReturn(
                <<<__JSON__
{"user": [{"name": "alex", "mobile": "+86 18900000000", "gender": "female", "created_at": "2014-03-17T22:34:35.400114", "activated_at": "2014-04-17T22:34:35.400114", "email": "alex@example.com", "portrait": "da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg", "nickname": "Alexandra", "id": 7}], "error": 0}
__JSON__
            )
        ;

        $this->inspect(array( 'alex', 'bob' ))->shouldBeLike(array(
            (new User( 7 ))
            ->setName( 'alex' )
            ->setMobile( '+86 18900000000' )
            ->setEmail( 'alex@example.com' )
            ->setNickname( 'Alexandra' )
            ->setPortrait( 'da39a3ee5e6b4b0d3255bfef95601890afd80709.jpg' )
            ->setGender( false )
            ->setCreatedAt(new \DateTime( '2014-03-17T22:34:35.400114' ))
            ->setActivatedAt(new \DateTime( '2014-04-17T22:34:35.400114' ))
        ));
    }
}
